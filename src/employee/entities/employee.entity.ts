import { User } from '../../user/entities/user.entity';
import {
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
} from 'typeorm';
@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  lastName: string;

  @Column()
  firstName: string;

  @Column()
  extensions: string;

  @Column({ unique: true })
  email: string;

  @Column()
  officeCode: string;

  @Column({ unique: true })
  reportTo: string;

  @OneToOne(() => User, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  user: User;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
