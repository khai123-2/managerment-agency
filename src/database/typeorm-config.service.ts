import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { AllConfigType } from 'src/config/config.type';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  constructor(private configService: ConfigService<AllConfigType>) {}

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: this.configService.get('database', { infer: true })?.type,
      host: this.configService.get('database', { infer: true })?.host,
      port: this.configService.get('database', { infer: true })?.port,
      username: this.configService.get('database', { infer: true })?.username,
      database: this.configService.get('database', { infer: true })?.name,
      password: this.configService.get('database', { infer: true })?.password,
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      migration: [__dirname + '/migrations/**/*.{.ts,.js}'],
    } as TypeOrmModuleOptions;
  }
}
