import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { FindOptionsWhere, Repository } from 'typeorm';
import { CreateUserDto } from './dtos/create-user.dto';
import { generateHash } from '@/common/utils';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    const existUsers = await this.getUsers({
      username: createUserDto.username,
    });
    if (existUsers.length > 0) {
      throw new BadRequestException('Username already exists ');
    }
    const hashedPassword = await generateHash(createUserDto.password);
    const newUser = await this.userRepository.save({
      ...createUserDto,
      password: hashedPassword,
    });
    delete newUser.password;
    return newUser;
  }

  async getUsers(fields: FindOptionsWhere<User>): Promise<User[] | []> {
    return await this.userRepository.find({ where: fields });
  }
}
