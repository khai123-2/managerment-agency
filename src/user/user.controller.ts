import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import { UserService } from './user.service';
import { Response, query } from 'express';
import { CreateUserDto } from './dtos/create-user.dto';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Get()
  async getUsers(@Query() query, @Res() res: Response) {
    const users = await this.userService.getUsers({ ...query });
    return res.status(HttpStatus.OK).send({ data: users });
  }
  @Post('register')
  async createUser(@Body() body: CreateUserDto, @Res() res: Response) {
    const user = await this.userService.createUser(body);
    return res.status(HttpStatus.OK).send({ data: user });
  }
}
